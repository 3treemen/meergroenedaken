<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MeerGroeneDaken
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="koffie">
			<h1>Koffie?</h1>
			<p>Neem gerust contact met me op als u nieuwsgierig bent geworden. 
				Ik kom dan graag bij u langs om uw wensen door te spreken.</p>
			<div class="icons">
				<div class="icon">
					<img src="/wp-content/themes/meergroenedaken/images/map-marker-alt-solid.svg" alt="locatie" />
					<span class="contact-meta">Onder de Linden, Arnhem</span>
				</div>
				<div class="icon">
					<a href="mailto:info@meergroenedaken.nl">
						<img src="/wp-content/themes/meergroenedaken/images/envelope-white.svg" alt="e-mail" />
						<span class="contact-meta">info@meergroenedaken.nl</span>
					</a>
				</div>
				<div class="icon">
					<a href="tel:+31653422661">
						<img src="/wp-content/themes/meergroenedaken/images/phone-alt-solid.svg" alt="telefoon" />
						<span class="contact-meta">06 53 42 26 61</span>
					</a>
				</div>
			</div>
		</div><!-- //koffie -->	
		<div id="contact-form">
			<?php echo do_shortcode('[contact-form-7 id="95" title="Contact form 1"]'); ?>
		</div>

	</footer><!-- #colophon -->
	<div id="re">
	<p>© <a href="http://www.zoo.nl">studio ZOO</a> - All rights reserved</p>
</div>
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>

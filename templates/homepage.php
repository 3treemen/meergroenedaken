<?php
/**
* Template Name: Home Page
*
* @package WordPress
* @subpackage meergroenedaken
* @since Meer Groene Daken
*/

get_header();
?>
<a id="vhg" href="https://www.vhg.org/">
		<img src="wp-content/themes/meergroenedaken/images/lid-van-VHG-kleur-links.png" alt="Lid van VHG" />
</a>
<main id="primary" class="site-main">
	<div class="container center">
	<h1>Subsidieregeling Initiatieven Klimaatadaptatie - gemeente Arnhem</h1>
	<p>Snel weten welk percentage subsidie u kunt krijgen? Mail dan uw adres èn dakoppervlak naar: <a href="mailto:info@meergroenedaken.nl">info@meergroenedaken.nl</a>
		</p>
	</div>

		<?php
		if ( have_posts() ) :
		
			echo '<h1>Nieuws</h1>';
			echo '<div id="post-listing">';
			meergroenedaken_post_listing(3);
			echo '</div>';

        endif;
		?>
		</main>
		
		<div id="wiebenik">

			<h1>Wie ben ik</h1>
			<div class="icons">
			<figur>
					<img class="img-responsive" src="wp-content/themes/meergroenedaken/images/wiebenik.png" alt="Joke Hofstee">
			</figure>
			</div>	
			<p>
			Welkom! Ik ben Joke Hofstee. Mijn passie is innovatie in verduurzaming. Met een technische èn culturele achtergrond vind ik het raakvlak van esthetiek en innovatie interessant. Groene daken zijn een praktisch en slim antwoord op klimaatadaptatie. En het is nog eens mooi ook!
			</p>

			<div class="social-icons">
				<a class="social" href="mailto:info@meergroenedaken.nl">
					<img src="/wp-content/themes/meergroenedaken/images/envelope-regular.svg" />
				</a>
				<a class="social" href="https://www.linkedin.com/in/jokehofstee1">
					<img src="/wp-content/themes/meergroenedaken/images/linkedin-in-brands.svg" />
				</a>
			</div>

		</div>


<?php
get_sidebar();
get_footer();

<?php
/**
* Template Name: Post Listing
*
* @package WordPress
* @subpackage meergroenedaken
* @since Meer Groene Daken
*/

get_header();
?>

	<main id="primary" class="site-main">

		<?php
		if ( have_posts() ) :

        		?>
				
				<?php
		

            meergroenedaken_post_listing(-1);
        endif;
		?>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
